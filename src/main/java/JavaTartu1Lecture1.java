package src.main.java;

//import org.graalvm.util.EconomicMap;

//import jdk.internal.util.xml.impl.Input;

//import com.sun.deploy.security.SelectableSecurityManager;

import java.util.Scanner;


public class JavaTartu1Lecture1 {

    static int age = 10;
    static String name = "Saeed";
    static float money = 10.7F;
    static double moneys = 10.7F;
    static char s;


    public static void main(String[] args) {
       /* System.out.println("Hey there!");
        System.out.println("Hey there!");
        System.out.println("Hey\nthere!");
        System.out.println(age);
        System.out.println(name);
        System.out.println(money);
        System.out.println(moneys);
        System.out.println(s);
        System.out.printf("%X:%X:%X:%X", 192, 168, 1, 10);

        int x = 4;
        System.out.println(x++);
        System.out.println(--x);
        System.out.println(x % 3);
        System.out.println(11 % 2);
        System.out.println(7 % x++);
        System.out.println(x == 4);
        System.out.println(x != 4);
        x = 10;
        int y = 5;
        System.out.println(x == 10 && y <= 5);
        System.out.println(x <= y && y > 5);
        System.out.println("abc" instanceof String);
    **/
        //HM1

        /*String lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";
        String loremLower = lorem.toLowerCase();
        System.out.println("Lorem lower case is ... " + loremLower);
        String loremUpper = lorem.toUpperCase();
        System.out.println("Lorem upper case is ... " + loremUpper);
        String loremReplace = lorem.replace("o", "z");
        System.out.println("Lorem replace is ... " + loremReplace);
        String loremEndswith = String.valueOf(lorem.endsWith("elit"));
        System.out.println("Lorem ends with ... " + loremEndswith);


        String a = "abc";
        String b = "abc";
        String c = new String("abc");
        System.out.println(a == b); //expected result
        System.out.println(a.equals(b)); //expected result
        System.out.println(b == c); //expected true ???
        System.out.println(b.equals(c));//expected result
*/
    /**
        //Casting
        double y1 = 49.543;
        int x1 = (int) y1;
        System.out.println("This is casting...  " + x1);

        *//** char charAt(int index)
         int compareTo(String other)
         boolean endsWith(String suﬀix)
         boolean equals(Object other)
         boolean equalsIgnoreCase(String other)
         int indexOf(String str)
         int lastIndexOf(String str)
         int length()
         String replace(CharSequence oldString,  CharSequence newString)
         boolean startsWith(String prefix)
         String substring(int beginIndex)
         String toLowerCase()
         String toUpperCase()
         String trim()
         **//*


        Scanner input = new Scanner(System.in);
        int number = Integer.parseInt(input.nextLine());
        // System.out.println(number);
        if (number == 4) {
            System.out.println("Yeahhhhhhhhhh!");
        } else if (number == 5) {
            System.out.println("Number is 5");
        } else {
            System.out.println("Number is " + number);
        }


        switch (number) {
            case 4:
                System.out.println("Yeahhhhhhh!");
                break;
            case 5:
                System.out.println("This should be 5!!");
                break;
            default:
                System.out.println("Yeahhhhhhhhhh" + number);
        }


        // LOOPS
        //While
        boolean a1 = true;
        while (a1) {
            System.out.println("a == true");
            a1 = false;
        }

        //int xx = 4;
        //For loop
        for (int xx = 4; xx < 7; xx++) {
            System.out.println("For loop is now  " + xx);
        }

        //Do while
        int num = 4;
        do {
            System.out.println(num);
            num++;
        } while (num > 6);

        do {
            System.out.println(num);
            num--;
        } while (num > 1);


        /// ARRAYS
        int[] intArrays = {1, 2, 3, 4, 5, 6, 7};
        // for(int i= 0; i<intArrays.length, )
        //enhanced loop
        for (int numb : intArrays) {
            System.out.println(numb);

        }

        int[] newArrays = new int[5];
        intArrays[0] = 4;
        System.out.println(intArrays[0]);


        //OOP
        Animal cat = new Animal("Cat", "inhale", "eat");
        Animal dog = new Animal("Cat", "inhale", "eat");

        cat.printType();
        cat.setBreathe();
        cat.setEat();

        dog.printType();
        dog.setBreathe();
        dog.setEat();


        System.out.println("This is before the time...debugging" + "\n");
        System.out.println(new Date().getTime());*/

        //printRectangleEdges();
        //UserPrintRectangleEdges();
        //UserEXPrintRectangleEdges();
      /*  try {
            UserEXPrintRectangleEdges2();
        } catch (RuntimeException ex) {
            System.out.println("There was an error, pleas try again!");
            UserEXPrintRectangleEdges2();
        }*/
       /* try {
            UserEXPrintRectangleEdges3();
        } catch (RuntimeException ex) {
            System.out.println("There was an error, pleas try again!");
            UserEXPrintRectangleEdges3();
        }*/
       /* try {
            UserEXPrintingSquare();
        } catch (RuntimeException ex) {
            System.out.println("There was an error, pleas try again!");
            UserEXPrintingSquare();
        }*/

       /* try {
            UserEXPrintingSquareEVEN();
        } catch (RuntimeException ex) {
            System.out.println("There was an error, pleas try again!");
            UserEXPrintingSquareEVEN();
        } */
       /* try {
            FibonacciSeries();
        } catch (RuntimeException ex) {
            System.out.println("There was an error, pleas try again!");
            FibonacciSeries();
        }*/

       //FibonacciSeriesUSERS();
       //FibonacciSeriesRecursion(7);

//        Animal[] animals  = createAnimals();
//        for (int index = 0; index < animals.length; index++){
//            Animal animal = animals[index];
//        }

        int[] values = createIntArrays();
        int sum = 0;
        for (int value : values){
            sum = sum + value;
        }
        System.out.println(sum);

        //Ask user for input
        //int number = 9;
        //System.out.println(calculateFactorial(number));

        //Factorial using loop
        /*int number = 6;
        System.out.println(calculateFactorialLoop(number));*/


        //Arrays multi-dimensional
       /* int[][] numbers = new int[][]{
                {1, 2, 3, 9, 5, 6, 7}, {1, 2, 3, 4, 7, 6, 7}
        };
        System.out.println(numbers.length);
        */
        //Idempotent(add zero to any number and no changes, multiple one by any number and no changes)
        /*int summ = 1;
        for (int[] number: numbers){
            summ += calculateArraySum(number);
            System.out.println("This is summ:  " + summ);
        }*/


        //Multiplication Table for multi Arrays

        int[][] multiplicationTable = new int[9][9];
        for (int row = 0; row < 9; row++) {
            for (int column = 0; column < 9; column++) {
               int multivalue = row * column;
                multiplicationTable[row][column] = multivalue;
            }
        }
        int row = 0;
        System.out.print("Columns: |");
        for (int column = 0; column < multiplicationTable[1].length; column++) {
            System.out.print(column + " | ");
        }
        System.out.println();
        for (int[] multiplication : multiplicationTable) {
            System.out.print(" Row =  " + row);
            for (int mulValue : multiplication) {
                System.out.print(" | ");
                System.out.print(mulValue);
                System.out.print(" | ");
            }
            row++;
            System.out.println();
        }


        //System.out.println(calculateArraySum(int[] numbers));

    }


    private static void printRectangleEdges() {
        int number = 4;
        for (int row = 0; row < number; row++) {
            System.out.print("*");
        }
        System.out.println();
        for (int column = 0; column < number - 1; column++) {
            System.out.print("*");
            for (int space = 0; space < number - 2; space++) {
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.println();
        }
        for (int row = 0; row < number; row++) {
            System.out.print("*");
        }
    }

    private static void UserPrintRectangleEdges() {
        Scanner input = new Scanner(System.in);
        int numbers = Integer.parseInt(input.nextLine());
        for (int row = 0; row < numbers; row++) {
            System.out.print("*");
        }
        System.out.println();
        for (int column = 0; column < numbers - 1; column++) {
            System.out.print("*");
            for (int space = 0; space < numbers - 2; space++) {
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.println();
        }
        for (int row = 0; row < numbers; row++) {
            System.out.print("*");
        }
    }

    private static void UserEXPrintRectangleEdges() {
        try {
            System.out.println("PLEASE ENTER A NUMBER:  ");
            Scanner numberOfRows = new Scanner(System.in);
            String inputFromUser = numberOfRows.nextLine();

            int noOfRows = Integer.parseInt(inputFromUser);
            if (inputFromUser.isEmpty()) {
                System.out.println("PLEASE ENTER A NUMBER:  ");
                //UserEXPrintRectangleEdges();
                return;
            }
            for (int row = 0; row < noOfRows; row++) {
                System.out.print("*");
            }
            System.out.println();
            for (int column = 0; column < noOfRows - 1; column++) {
                System.out.print("*");
                for (int space = 0; space < noOfRows - 2; space++) {
                    System.out.print(" ");
                }
                System.out.print("*");
                System.out.println();
            }
            for (int row = 0; row < noOfRows; row++) {
                System.out.print("*");
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("The red texts above are exceptions!");
        }
    }

    // Using the TRY/CATCH method outside the function.
    private static void UserEXPrintRectangleEdges2() {

        System.out.println("PLEASE ENTER A NUMBER:  ");
        Scanner numberOfRows = new Scanner(System.in);
        int noOfRows = numberOfRows.nextInt();
        for (int row = 0; row < noOfRows; row++) {
            System.out.print("*");
        }
        System.out.println();
        for (int column = 0; column < noOfRows - 1; column++) {
            System.out.print("*");
            for (int space = 0; space < noOfRows - 2; space++) {
                System.out.print(" ");
            }
            System.out.print("*");
            System.out.println();
        }
        for (int row = 0; row < noOfRows; row++) {
            System.out.print("*");
        }
    }

    // Setting
    private static void UserEXPrintRectangleEdges3() {

        System.out.println("PLEASE ENTER A NUMBER:  ");
        Scanner numberOfRows = new Scanner(System.in);
        if (numberOfRows.hasNextInt()) {
            int noOfRows = numberOfRows.nextInt();
            for (int row = 0; row < noOfRows; row++) {
                System.out.print("*");
            }
            System.out.println();
            for (int column = 0; column < noOfRows - 1; column++) {
                System.out.print("*");
                for (int space = 0; space < noOfRows - 2; space++) {
                    System.out.print(" ");
                }
                System.out.print("*");
                System.out.println();
            }
            for (int row = 0; row < noOfRows; row++) {
                System.out.print("*");
            }
        } else {
            UserEXPrintRectangleEdges3();
        }
    }

    //Get the number of Rows and columns from the users and compare them
    // before executing the rest of the code
    private static void UserEXPrintingSquare() {

        System.out.println("Please enter a number for row:  ");
        Scanner scanners = new Scanner(System.in);
        int noOfRows = scanners.nextInt();

        System.out.println("Please enter a number for column:  ");
        int noOfColumns = scanners.nextInt();

        if (noOfRows != noOfColumns) {
            System.out.println("Please, enter equal number of rows and columns");
            System.out.println();
            UserEXPrintingSquare();
            return;
        }

        for (int row = 0; row < noOfRows; row++) {
            System.out.println();
            for (int column = 0; column < noOfColumns; column++) {
                System.out.print("*");
            }
        }
    }

    //Check for even Rows and columns from the users
    // before executing the rest of the code
    private static void UserEXPrintingSquareEVEN() {

        System.out.println("Please enter a number for row:  ");
        Scanner scanners = new Scanner(System.in);
        int noOfRows = scanners.nextInt();
        if(noOfRows % 2 != 0){
            System.out.println("Please, enter an even number of rows!");
            System.out.println();
            UserEXPrintingSquareEVEN();
            return;
        }

        System.out.println("Please enter a number for column:  ");
        int noOfColumns = scanners.nextInt();
        if(noOfColumns % 2 != 0){
            System.out.println("Please, enter an even number for columns!");
            System.out.println();
            UserEXPrintingSquareEVEN();
            return;
        }

        if (noOfRows != noOfColumns) {
            System.out.println("Please, enter equal number of rows and columns");
            System.out.println();
            UserEXPrintingSquare();
            return;
        }

        for (int row = 0; row < noOfRows; row++) {
            System.out.println();
            for (int column = 0; column < noOfColumns; column++) {
                System.out.print("*");
            }
        }
    }

    //Using Fibonacci sequence
    private static void FibonacciSeries() {
        int fibo = 0;
        int fibo1 = 1;
        int sum = fibo + fibo1;
        System.out.println(sum);
        for (int i = 1; i < 10; i++) {
            System.out.println(sum);
            fibo = fibo1 ;
            fibo1 = sum ;
            sum = fibo + fibo1;
        }
    }

    private static void FibonacciSeriesUSERS() {
        System.out.println("Please enter your Fibonacci number:   ");
        Scanner scanners = new Scanner(System.in);
        int fiboNumber = scanners.nextInt();
        int fibo = 0;
        int fibo1 = 1;
        int sum = fibo + fibo1;
        System.out.println(sum);
        for (int i = 1; i < fiboNumber; i++) {
            System.out.println(sum);
            fibo = fibo1 ;
            fibo1 = sum ;
            sum = fibo + fibo1;
        }
    }


    //Fibonacci sequence without a For Loop using RECURSION
    private static void FibonacciSeriesRecursion(int lenghtOfSeries) {
        if(lenghtOfSeries == 0){
            return;
        }
        System.out.println(lenghtOfSeries);
        FibonacciSeriesRecursion(--lenghtOfSeries);
    }

    private static int fibonacciSeriesRecursion(int lengthOfSeries) {
        System.out.println("This is Fibonacci Series using Recursion...");
        if(lengthOfSeries == 1| lengthOfSeries == 2){
            return 1;
        }
        return fibonacciSeriesRecursion(lengthOfSeries -2)
                + fibonacciSeriesRecursion(lengthOfSeries - 1);
    }


    // Creating ARRAYS...
    public static  Animal[] createAnimals(){
        Animal[]  animals = new Animal[5];
        /*animals[0] = new Animal("dog","breathe", "eat");
        animals[1] = new Animal("dog1","breathe", "eat");
        animals[2] = new Animal("dog2","breathe", "eat");
        animals[3] = new Animal("dog3","breathe", "eat");
        animals[4] = new Animal("dog4","breathe", "eat");*/
        //using for loop
        for (int index = 0; index < 5; index++)
            animals[index]= new Animal("dog "+index + 1, "Breathe " + index, "Eat " + index);
        return animals;
    }

    private static int[] createIntArrays(){
        int[]values = new int[15];
        for (int index = 0; index < 15; index++){
            values[index] = index + 1;
        }
        return values;
    }



    ///FACTORIAL
    //Take home recursion
    private static int calculateFactorial(int number){
        if(number ==0){
            return 1;
        }
        return number * calculateFactorial(number - 1);
    }

    //Factorial using loop
    private static int calculateFactorialLoop(int number){
        int sum = 1;
        for (int i = number; i >0; i--){
            sum *= i; //sum = sum * number;
        } return sum;
    }


    //ARRAYS
    private static int calculateArraySum(int[] numbers){
        int sum = 0;
        int index = 0;
        while (index < numbers.length){
            int  value = numbers[index];
            sum += value;
            index++;
        } return sum;
    }


}